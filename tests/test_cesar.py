import projet_encodage
import unittest
from projet_encodage import encodage
from projet_encodage import decodage

class TestEncodageDecodage(unittest.TestCase):

    def test_encodage(self):
        self.assertEqual("IJKL", encodage("ABCD", 8))
        self.assertEqual("Rm({}q{)", encodage("Je suis!", 8))
        self.assertEqual("939E;", encodage("1+1=3", 8))
<<<<<<< HEAD
        self.assertEqual("INFORMATIQUE", encodage("AF<GJE9LAIM=", 8))
=======
        self.assertEqual("INFORMATIQUE", encodage("AF>GJE9LAIM=", 8))
>>>>>>> 41d3dbed5929fe17be61f31043427b0720bbaae4
    
    def test_decodage(self):
        self.assertEqual("AF>GJE9LAIM=", decodage("INFORMATIQUE", 8))
        self.assertEqual("1+1=3", decodage("939E;", 8))
        self.assertEqual("ABCD", decodage("IJKL", 8))
        self.assertEqual("Je suis!", decodage("Rm({}q{)", 8))
