from tkinter import  * 
from tkinter.messagebox import * 
from tkinter.font import * 
import string 

def circ_decode(String, k) : 
     result= ""
     for c in String:
          if (ord(c)>=ord('A') & ord(c)<=ord('Z')) | (ord(c)>=ord('a') & ord(c)<=ord('z')) :
               result= result + (chr( (ord(c)-k) % 26))
          else :
               result = result + c
     return result

def circ_encode(String, k) : 
     result= ""
     for c in String:
          if (ord(c)>=ord('A') & ord(c)<=ord('Z')) | (ord(c)>=ord('a') & ord(c)<=ord('z')) :
               result= result + (chr( (ord(c)+k) % 26))
          else :
               result = result + c
     return result



root = Tk()
root.title("Projet codage")
root.geometry("700x500")
root.config(background="#f0f0f0")

bouton = Frame(root, bd=1, pady=10) #boîte englobante des boutons codage et decpage 
lbl = Label(bouton, text="Decalage : ") 
decalage = Entry(bouton, bg ="bisque", fg="maroon", width="10")


decalage.grid(row=0, column=4, sticky='', padx=10)
lbl.grid(row=0, column=3, sticky='')


def decodage(String) : 
     k = int(decalage.get())  #pas de decalage 
     result= ""
     for c in String:
          result= result + (chr(ord(c)-k))
     return result
           

def decoder() : 
     k = int(decalage.get() )
     message_transforme.text = decodage(zone_text.get("0.0", END))
    # zone_text.delete("0.0", END)
     showinfo('DECODER',  ' votre message : \n %s  ' % message_transforme.text)  

"""cette fonction prend le texte saisie en dans le tableau et retourne un message dans 
codé.
le message codé est décaler de 8 caractère en fonction de le nombre inscrit dans la 
table ASCII 
"""
def encodage(String) : 
     k = int(decalage.get())
     result= ""
     for c in String:
          result= result + (chr(ord(c)+k))
     return result
           
def encoder() : 
     k =int(decalage.get())
     message_transforme.text = encodage(zone_text.get("0.0", END))
    # zone_text.delete("0.0", END)
     showinfo('ENCODER',  ' votre message : \n %s  ' % message_transforme.text)   


#lable du titre  sur l'application crée 
titre= Label(root, text="ENCODAGE MESSAGE PAR CESAR ", font=("courrier", 15))

#zone de saisie du message à transcrire 
zone_text = Text(root, font=("Arial", 10))

btn_coder = Button(bouton, text="CODER !", command=encoder, font=("Arial", 10) ) 
btn_coder.grid(row=0, column=0, sticky='', padx=20)
btn_decoder = Button(bouton, text="DECODER !" , command = decoder, font=("Arial", 10))
btn_decoder.grid(row=0, column=1 , sticky='', padx=20)
btn_quitter = Button(bouton, text="QUITTER !" , command = root.destroy, font=("Arial", 10))
btn_quitter.grid(row=0, column=2 , sticky='', padx=20)
message_transforme = Label(root, text="", font=("courrier",15 ))

choix = Frame(root, bd=1, pady = 10)    #frame qui contient les cases à cocher 
activation = IntVar()   # 0 pour non couche ou 1 pour coché 
non_cir = IntVar()
circulaire = Checkbutton(choix, text="Circulaire", variable = activation)
non_circulaire = Checkbutton(choix, text="Non circulaire", variable= non_cir)
circulaire.grid(row=1, column=0)
non_circulaire.grid(row=1, column=1)
if activation.get() == non_cir.get() == 1:
     btn_coder["state"] = "disabled"
     btn_decoder["state"] = "disabled"







titre.pack(side=TOP)
zone_text.pack(expand=YES)
bouton.pack(expand=YES, side=BOTTOM)
choix.pack(expand=YES)
message_transforme.pack(expand=YES)


root.mainloop()